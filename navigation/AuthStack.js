import 'react-native-gesture-handler';
import React from 'react';
import {View} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import { Icon } from "react-native-elements";

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{header: () => null}}/>
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={({navigation})=>({
          title: '',
          headerStyle: {
            backgroundColor: 'transparent',
            shadowColor: 'transparent',
            elevation: 0,
          },
          headerLeft: () => (
            <View style={{marginLeft: 10}}>
              <Icon
                onPress={() => navigation.navigate('Login')}
                name="subdirectory-arrow-left"
                color="#F4770B"
              />
            </View>
          ),
        })}/>
    </Stack.Navigator>
  )
}

export default AuthStack;