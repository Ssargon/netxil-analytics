import React, {createContext, useState} from 'react';
import * as firebase from "firebase";

export const AuthContext = createContext();

const AuthProvider = ({children}) => {
  const [user, setUser] = useState(null);
  return (
    <AuthContext.Provider
      value={{
        user,
        setUser,
        login: async (email, password) => {
          try {
            await firebase.default.auth().signInWithEmailAndPassword(email, password)
              .then(user => console.log(user));
          } catch (err) {
            alert(err);
          }
        },
        register: async (email, password) => {
          try {
            await firebase.default.auth().createUserWithEmailAndPassword(email, password)
              .then(user => console.log(user));
          } catch (err) {
            alert(err);
          }
        },
        logout: async () => {
          try {
            await firebase.default.auth().signOut();
          } catch (err) {
            alert(err);
          }
        }
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export default AuthProvider;