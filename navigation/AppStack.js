import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import WordsScreen from "../screens/WordsScreen";
import IrregularVerbsScreen from "../screens/IrregularVerbsScreen";
import RadialMenu from "../components/common/RadialMenu";

const Stack = createStackNavigator();

const AppStack = () => {
  return (
    <Stack.Navigator initialRouteName="List" screenOptions={{animationEnabled: false}}>
      <Stack.Screen
        name="List"
        component={WordsScreen}
        options={
          ({navigation}) => ({
            header: () => <RadialMenu goToPage={(page) => navigation.navigate(page)}/>
          })
        }/>
      <Stack.Screen
        name="Irregular"
        component={IrregularVerbsScreen}
        options={
          ({navigation}) => ({
            header: () => <RadialMenu goToPage={(page) => navigation.navigate(page)}/>
          })
        }/>
    </Stack.Navigator>

  );
}

export default AppStack;
