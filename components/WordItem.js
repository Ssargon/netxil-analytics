import React, {useState} from 'react';
import {Alert, Pressable, StyleSheet, Text, View} from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";

const WordItem = ({word, deleteWord}) => {
  const [isTranslated, setTranslated] = useState(false);

  const deleteAction = () => {
    Alert.alert(`${word.text}`, 'Are you sure you want to delete', [
      {text: 'Delete', onPress: () => deleteWord(word.id)},
      {cancelable: false}
    ]);
  }

  return (
    <View style={styles.wrapper}>
      <Text style={styles.text}>{isTranslated ? word.translate : word.text}</Text>
      <View style={styles.buttonsWrapper}>
        <Pressable onPress={() => setTranslated(!isTranslated)}>
          <AntDesign
            name="google"
            size={30}
            color="#F4770B"/>
        </Pressable>
        <Pressable onPress={() => deleteAction()}>
          <AntDesign
            name="closesquareo"
            size={30}
            color="#F62915"/>
        </Pressable>
      </View>
    </View>
  )
}

export default WordItem;

const styles = StyleSheet.create({
  text: {
    fontSize: 15,
    color: '#321A31',
  },
  wrapper: {
    backgroundColor: '#E1E1E4',
    padding: 5,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 5,
    marginBottom: 5,
    borderRightColor: '#321A31',
    borderRightWidth: 2,
    borderLeftColor: '#321A31',
    borderLeftWidth: 2,
  },
  buttonsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    width: 70,
    justifyContent: 'space-between',
  }
});