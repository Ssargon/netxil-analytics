import React from 'react';
import {StyleSheet, SafeAreaView, StatusBar, ScrollView} from "react-native";

const ScreenContentWrapper = ({children}) => {
  return (
    <SafeAreaView style={styles.areaView}>
      <ScrollView style={styles.scrollView}>
        {children}
      </ScrollView>
    </SafeAreaView>
  )
}

export default ScreenContentWrapper;

const styles = StyleSheet.create({
  scrollView: {
    marginHorizontal: 10,
  },
  areaView: {
    paddingTop: 10,
    paddingBottom: 10,
  }
});