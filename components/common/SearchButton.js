import React from 'react';
import AntDesign from "react-native-vector-icons/AntDesign";
import {Pressable, StyleSheet} from "react-native";

const SearchButton = ({onClick}) => {
  return (
    <Pressable style={styles.buttonWrapper} onPress={() => onClick()}>
      <AntDesign
        style={styles.button}
        name="search1"
        size={35}
        color="#321A31"/>
    </Pressable>
  )
}

export default SearchButton;

const styles = StyleSheet.create({
  buttonWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4770B',
    borderRadius: 50,
    width: 50,
    height: 50,
  },
});