import React, {useContext, useState} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import AntDesign from "react-native-vector-icons/AntDesign";
import {AuthContext} from "../../navigation/AuthProvider";

const RadialMenu = ({goToPage}) => {
  const [isActive, setActive] = useState(false);
  const {logout} = useContext(AuthContext);
  return (
    <View>
      <TouchableOpacity style={style.menu} onPress={() => setActive(!isActive)}>
        <AntDesign
          style={style.downIcon}
          name={isActive ? "up" : "down"}
          size={50}
          color="#FFF"/>
      </TouchableOpacity>
      <TouchableOpacity
        style={isActive ? {...style.menuItem, ...style.listItem} : {...style.menuItem}}
        onPress={() => goToPage('List')}>
        <AntDesign name="bars" size={20} color="#FFF"/>
      </TouchableOpacity>
      <TouchableOpacity
        style={isActive ? {...style.menuItem, ...style.IrregularItem} : {...style.menuItem}}
        onPress={() => goToPage('Irregular')}>
        <AntDesign name="minuscircleo" size={20} color="#FFF"/>
      </TouchableOpacity>
      <TouchableOpacity
        style={isActive ? {...style.menuItem, ...style.logoutItem} : {...style.menuItem}}
        onPress={() => logout()}>
        <AntDesign name="export2" size={20} color="#FFF"/>
      </TouchableOpacity>
    </View>
  );
}

export default RadialMenu

const style = StyleSheet.create({
  menu: {
    width: 100,
    height: 100,
    backgroundColor: '#F4770B',
    position: 'relative',
    left: '50%',
    display: 'flex',
    alignItems: 'center',
    borderRadius: 50,
    transform: [
      {translateX: -50},
      {translateY: -50}
    ]
  },
  downIcon: {
    top: 40,
  },
  menuItem: {
    width: 50,
    height: 50,
    backgroundColor: '#FFF',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    position: 'absolute',
    left: '50%',

    transform: [
      {translateX: -25}
    ],
    zIndex: -1,
  },
  listItem: {
    backgroundColor: '#F4770B',
    left: '31%',
    top: '15%',
  },
  logoutItem: {
    backgroundColor: '#F4770B',
    left: '69%',
    top: '15%',
  },
  IrregularItem: {
    backgroundColor: '#F4770B',
    left: '50%',
    top: '58%',
  },
});
