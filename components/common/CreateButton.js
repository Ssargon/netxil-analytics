import React from 'react';
import AntDesign from "react-native-vector-icons/AntDesign";
import {Pressable, StyleSheet} from "react-native";

const CreateButton = ({onClick}) => {
  return (
    <Pressable onPress={() => onClick()}>
      <AntDesign
        style={styles.button}
        name="pluscircleo"
        size={50}
        color="#321A31"/>
    </Pressable>
  )
}

export default CreateButton;

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#F4770B',
    width:50,
    height:50,
    borderRadius:50,
  }
});