import React from 'react';
import {View, StyleSheet} from "react-native";
import {Row, Rows, Table} from "react-native-table-component";

const IrregularVerbs = ({irregulars}) => {
  const tableHead = ['Infinitive', 'Past Simple', 'Past Participle', 'Translate'];
  const convertedIrregular = irregulars.map(i => [i.infinitive, i.pastSimple, i.pastParticle, i.translate]);
  return (
    <View style={styles.container}>
      <Table borderStyle={{borderWidth: 2, borderColor: '#F4770B'}}>
        <Row data={tableHead} style={styles.head} textStyle={styles.text}/>
        <Rows data={convertedIrregular} textStyle={styles.text}/>
      </Table>
    </View>
  )
}

export default IrregularVerbs;

const styles = StyleSheet.create({
  container: {flex: 1},
  head: {height: 40, backgroundColor: '#E1E1E4'},
  text: {margin: 4}
});