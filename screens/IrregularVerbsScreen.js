import React, {useContext, useEffect, useState} from 'react';
import {View, Text, StyleSheet, Modal, TextInput, Pressable} from "react-native";
import {Inter_900Black, useFonts} from "@expo-google-fonts/inter";
import CreateButton from "../components/common/CreateButton";
import AntDesign from "react-native-vector-icons/AntDesign";
import * as firebase from "firebase";
import {AuthContext} from "../navigation/AuthProvider";
import ScreenContentWrapper from "../components/common/ScreenContentWrapper";
import IrregularVerbs from "../components/IrregularVerbs";

const IrregularVerbsScreen = () => {
  const {user} = useContext(AuthContext);
  const [showModal, setShowModal] = useState(false);
  const [reload, setReload] = useState(false);
  const [irregularVerbs, setIrregularVerbs] = useState([]);
  const [infinitive, setInfinitive] = useState('');
  const [pastSimple, setPastSimple] = useState('');
  const [pastParticle, setPastParticle] = useState('');
  const [textTranslate, setTextTranslate] = useState('');

  useEffect(() => {
    const getIrregularVerbs = async () => {
      let irregularVerbs = [];
      const data = (await firebase.default.database().ref(`/users/${user.uid}/irregularVerbs`).once('value'))
        .val();
      Object.keys(data).forEach(key => {
        irregularVerbs.push({
          id: key,
          infinitive: data[key].infinitive,
          pastSimple: data[key].pastSimple,
          pastParticle: data[key].pastParticle,
          translate: data[key].translate,
        });
      });
      setReload(false);
      setIrregularVerbs(irregularVerbs);
    }
    getIrregularVerbs();
  }, [reload]);

  let [fontsLoaded] = useFonts({
    Inter_900Black,
  });

  if (!fontsLoaded) {
    return null;
  }

  const resetFields = () => {
    setInfinitive('');
    setPastSimple('');
    setPastParticle('');
    setTextTranslate('');
  }

  const createIrregularVerb = async () => {
    await firebase.default.database().ref(`/users/${user.uid}/irregularVerbs`).push({
      infinitive: infinitive,
      pastSimple: pastSimple,
      pastParticle: pastParticle,
      translate: textTranslate,
    });
    setReload(true);
    resetFields();
    setShowModal(false);
  }

  if (reload) return (
    <View style={[styles.container, styles.horizontal]}>
      <Text>...</Text>
    </View>
  );

  return (
    <>
      <View style={styles.wrapper}>
        <View style={styles.headerWrapper}>
          <Text style={styles.bannerText}>Irregular</Text>
          <CreateButton onClick={() => setShowModal(true)}/>
        </View>
        <ScreenContentWrapper>
          <IrregularVerbs irregulars={irregularVerbs}/>
        </ScreenContentWrapper>
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showModal}
        onRequestClose={() => setShowModal(false)}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text>Add new Irregular Verb</Text>
            <TextInput
              style={styles.textInput}
              onChangeText={text => setInfinitive(text)}
              value={infinitive}
              placeholder='Infinitive'
            />
            <TextInput
              style={styles.textInput}
              onChangeText={text => setPastSimple(text)}
              value={pastSimple}
              placeholder='Past Simple'
            />
            <TextInput
              style={styles.textInput}
              onChangeText={text => setPastParticle(text)}
              value={pastParticle}
              placeholder='Past Participle'
            />
            <TextInput
              style={styles.textInput}
              onChangeText={text => setTextTranslate(text)}
              value={textTranslate}
              placeholder='Translate'
            />
            <View style={styles.buttonsModalWrapper}>
              <Pressable onPress={() => createIrregularVerb()}>
                <AntDesign
                  name="pluscircleo"
                  size={30}
                  color="#F4770B"/>
              </Pressable>
              <Pressable style={styles.closeBtn} onPress={() => {
                resetFields();
                setShowModal(false)
              }}>
                <AntDesign
                  name="closecircleo"
                  size={30}
                  color="#321A31"/>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    </>
  )
}

export default IrregularVerbsScreen;

const styles = StyleSheet.create({
  wrapper: {
    paddingRight: '3%',
    paddingLeft: '3%',
    position: 'relative',
    top: -95,
  },
  bannerText: {
    fontSize: 20,
    color: '#321A31',
    fontFamily: 'Inter_900Black',
  },
  headerWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textInput: {
    height: 40,
    borderBottomColor: '#321A31',
    borderBottomWidth: 2,
    marginBottom: 10,
  },
  buttonsModalWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    shadowColor: "#000",
    width: 200,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  closeBtn: {
    marginLeft: 10,
  },
});