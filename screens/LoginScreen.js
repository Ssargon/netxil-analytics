import React, {useContext, useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {useFonts, Inter_900Black} from '@expo-google-fonts/inter';
import FormInput from "../components/common/FormInput";
import FormButton from "../components/common/FormButton";
import {AuthContext} from '../navigation/AuthProvider';

const LoginScreen = ({navigation}) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const {login} = useContext(AuthContext);

  let [fontsLoaded] = useFonts({
    Inter_900Black,
  });

  if (!fontsLoaded) {
    return null;
  }

  return (
    <View>
      <View style={styles.wrapper}>
        <Image style={styles.image} source={require('../assets/images/eladmizrahi-Super-Dude.png')}/>
        <Text style={styles.headerText}>English Man</Text>
        <FormInput
          labelValue={email}
          onChangeText={(userEmail) => setEmail(userEmail)}
          placeholderText="Email"
          iconType="user"
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
        />
        <FormInput
          labelValue={password}
          onChangeText={(userPassword) => setPassword(userPassword)}
          placeholderText="Password"
          iconType="lock"
          secureTextEntry={true}
        />
        <FormButton
          buttonTitle="Sign In"
          onPress={() => login(email, password)}
        />
        <View style={styles.formFooter}>
          <Text>Don't have an account?</Text>
          <TouchableOpacity style={styles.registerBtn} onPress={() => navigation.navigate('Register')}>
            <Text style={styles.registerBtnText}>Register</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  image: {
    height: 200,
    width: 170,
    resizeMode: 'stretch',
  },
  headerText: {
    fontSize: 25,
    color: '#F4770B',
    fontFamily: 'Inter_900Black',
  },
  wrapper: {
    paddingLeft: '15%',
    paddingRight: '15%',
    top: '20%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  formFooter: {
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
  },
  registerBtn: {
    marginLeft: 5,
  },
  registerBtnText: {
    color: '#F4770B',
    fontWeight: 'bold',
  }
});

export default LoginScreen;