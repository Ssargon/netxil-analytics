import React, {useContext, useEffect, useState} from 'react';
import {View, Text, StyleSheet, Modal, Pressable, TextInput, ActivityIndicator} from "react-native";
import {Inter_900Black, useFonts} from "@expo-google-fonts/inter";
import {AuthContext} from "../navigation/AuthProvider";
import * as firebase from "firebase";
import CreateButton from "../components/common/CreateButton";
import AntDesign from "react-native-vector-icons/AntDesign";
import ScreenContentWrapper from "../components/common/ScreenContentWrapper";
import WordItem from "../components/WordItem";
import SearchButton from "../components/common/SearchButton";

const WordsScreen = () => {
  const {user} = useContext(AuthContext);
  const [reload, setReload] = useState(false);
  const [words, setWords] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [showSearchModal, setSearchModal] = useState(false);
  const [text, setText] = useState('');
  const [textTranslate, setTextTranslate] = useState('');
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    const getCategories = async () => {
      let words = [];
      const data = (await firebase.default.database().ref(`/users/${user.uid}/words`).once('value'))
        .val();
      Object.keys(data).forEach(key => {
        words.push({
          id: key,
          text: data[key].text,
          translate: data[key].translate,
        });
      });
      setReload(false);
      setWords(words);
    }
    getCategories();
  }, [reload]);

  let [fontsLoaded] = useFonts({
    Inter_900Black,
  });

  const createWord = async () => {
    await firebase.default.database().ref(`/users/${user.uid}/words`).push({
      text: text,
      translate: textTranslate
    });
    setReload(true);
    setText('');
    setTextTranslate('');
    setShowModal(false);
  }

  const search = async (text) => {
    if (searchText === '') return setReload(true);
    await firebase.default.database().ref(`/users/${user.uid}`)
      .child('words')
      .orderByChild('text')
      .equalTo(text)
      .on('value', (snapshot) => {
        const data = snapshot.val();
        if (data) Object.keys(data).forEach(key => {
          setWords([{
            id: key,
            text: data[key].text,
            translate: data[key].translate,
          }]);
        });
      });
  }

  const deleteWord = async (wordId) => {
    await firebase.default.database()
      .ref(`/users/${user.uid}/words/${wordId}`)
      .remove(() => setReload(true));

  }

  if (!fontsLoaded) {
    return null;
  }

  if (reload) return (
    <View style={[styles.container, styles.horizontal]}>
      <Text>...</Text>
    </View>
  );

  return (
    <>
      <View style={styles.wrapper}>
        <View style={styles.headerWrapper}>
          <Text style={styles.bannerText}>Vocabulary</Text>
          <View style={styles.buttonsWrapper}>
            <SearchButton onClick={() => setSearchModal(true)}/>
            <CreateButton onClick={() => setShowModal(true)}/>
          </View>
        </View>
        <ScreenContentWrapper>
          {words.map(word => <WordItem key={word.id} word={word} deleteWord={(wordId) => deleteWord(wordId)}/>)}
        </ScreenContentWrapper>
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showModal}
        onRequestClose={() => setShowModal(false)}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text>Add New Word</Text>
            <TextInput
              style={styles.textInput}
              onChangeText={text => setText(text)}
              value={text}
              placeholder='Text'
            />
            <TextInput
              style={styles.textInput}
              onChangeText={text => setTextTranslate(text)}
              value={textTranslate}
              placeholder='Translate'
            />
            <View style={styles.buttonsModalWrapper}>
              <Pressable onPress={() => createWord()}>
                <AntDesign
                  name="pluscircleo"
                  size={30}
                  color="#F4770B"/>
              </Pressable>
              <Pressable style={styles.closeBtn} onPress={() => {
                setText('')
                setTextTranslate('');
                setShowModal(false)
              }}>
                <AntDesign
                  name="closecircleo"
                  size={30}
                  color="#321A31"/>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showSearchModal}
        onRequestClose={() => setSearchModal(false)}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TextInput
              style={styles.textInput}
              onChangeText={text => setSearchText(text)}
              value={searchText}
              placeholder='Text'
            />
            <View style={styles.buttonsModalWrapper}>
              <Pressable onPress={() => search(searchText)}>
                <AntDesign
                  name="search1"
                  size={30}
                  color="#F4770B"/>
              </Pressable>
              <Pressable style={styles.closeBtn} onPress={() => {
                setSearchText('')
                setSearchModal(false)
              }}>
                <AntDesign
                  name="closecircleo"
                  size={30}
                  color="#321A31"/>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    </>
  )
}

export default WordsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  wrapper: {
    paddingRight: '3%',
    paddingLeft: '3%',
    position: 'relative',
    top: -95,
  },
  headerWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bannerText: {
    fontSize: 20,
    color: '#321A31',
    fontFamily: 'Inter_900Black',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    shadowColor: "#000",
    width: 200,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  buttonsModalWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  closeBtn: {
    marginLeft: 10,
  },
  textInput: {
    height: 40,
    borderBottomColor: '#321A31',
    borderBottomWidth: 2,
    marginBottom: 10,
  },
  buttonsWrapper: {
    display: 'flex',
    flexDirection: 'row',
    width: 110,
    justifyContent: 'space-between'
  }
});