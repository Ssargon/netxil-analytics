import React from 'react';
import {StyleSheet, View} from 'react-native';
import * as firebase from "firebase";
import Providers from "./navigation";

const firebaseConfig = {
  apiKey: "AIzaSyDf_tBdEGG0w18OI19YoZLTu1PivOtu-KQ",
  authDomain: "netxil-english-note.firebaseapp.com",
  projectId: "netxil-english-note",
  storageBucket: "netxil-english-note.appspot.com",
  messagingSenderId: "1092781138633",
  appId: "1:1092781138633:web:bce9c83bcf14b01dc46318",
  measurementId: "G-PJ9ZY4ZVST"
};

if (!firebase.default.apps.length) firebase.default.initializeApp(firebaseConfig);

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.topLine}/>
      <Providers/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
  },
  topLine: {
    height: '5%',
    backgroundColor: '#F4770B',
  }
});
